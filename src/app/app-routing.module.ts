import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NoauthComponent} from './noauth/noauth.component';
import {AuthComponent} from './auth/auth.component';
import {AuthGuard} from './auth.guard';

const routes: Routes = [
  { path: '', redirectTo: 'no-auth', pathMatch: 'full'},
  { path: 'no-auth', component: NoauthComponent },
  { path: 'auth', component: AuthComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
